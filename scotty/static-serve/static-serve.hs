{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty (middleware, scotty, get, file, param)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)

main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev

  get "/" $ file $ "./static/index.html"

  get "/:myfile" $ do
    myfile <- param "myfile"
    file $ "./static/" ++ myfile

