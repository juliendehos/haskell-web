-- sqlite3 todolist.db < todolist.sql

CREATE TABLE task (
  id INTEGER PRIMARY KEY,
  name TEXT
);

INSERT INTO task (name) VALUES('traire la piscine');
INSERT INTO task (name) VALUES('repeindre la chèvre');

