module Main exposing (..)

import Dom exposing (focus)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Task


main : Program Never Model Msg
main =
    Html.program
        { view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        , init = ( model, Cmd.none )
        }


type alias Model =
    { tasks : List String
    , entry : String
    }


model : Model
model =
    Model [] ""


type Msg
    = NewTask
    | NewEntry String
    | FocusResult (Result Dom.Error ())


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewTask ->
            let
                newModel =
                    if model.entry == "" then
                        model
                    else
                        Model ([ model.entry ] ++ model.tasks) ""
            in
            newModel
                ! [ Task.attempt FocusResult (focus "myinput") ]

        NewEntry str ->
            { model | entry = str } ! []

        FocusResult _ ->
            model ! []


view : Model -> Html Msg
view model =
    div []
        [ h1 []
            [ text "ma page web" ]
        , div
            []
            [ input
                [ id "myinput", onInput NewEntry, value model.entry ]
                []
            , button [ onClick NewTask ]
                [ text "ajouter" ]
            , ul []
                (List.map (\x -> li [] [ text x ]) model.tasks)
            ]
        ]
