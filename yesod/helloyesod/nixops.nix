{
  network.description = "helloyesod";

  helloyesod = { config, pkgs, ... }: 
  let 
    _helloyesod = import ./default.nix { inherit pkgs; };
  in
  { 
    networking.firewall.allowedTCPPorts = [ 3000 ];

    systemd.services.helloyesod = { 
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = { 
        ExecStart = "${_helloyesod}/bin/helloyesod";
      };
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}

