{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

import CodeCaesarText
import MusicPersist

import           Control.Applicative ((<$>), (<*>))
import           Control.Monad.IO.Class (liftIO)
import qualified Data.Text as T
import           Data.Maybe (maybe)
import           Yesod

port = 3000

data App = App

instance Yesod App

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

mkYesod "App" [parseRoutes|
  /         HomeR     GET
  /caesar   CaesarR   GET
  /music    MusicR    GET
  |]

myFooter = [whamlet| 
  <p>
    <a href=@{HomeR}> back home 
  |]

myCss = toWidget [cassius|
  .result
    border: 1px solid black
    background: rgb(255,220,150)
  div
    margin-bottom: 10px
  |]

getHomeR = defaultLayout $ do
  [whamlet|
    <h1> Home
    <p> 
      <a href=@{CaesarR}> Caesar 
    <p> 
      <a href=@{MusicR}> Music 
    |]

-- Ceasar

caesarForm :: Html -> MForm Handler (FormResult Caesar, Widget)
caesarForm = renderDivs $ Caesar
    <$> areq intField "Key" (Just 13)
    <*> areq textField "Msg" (Just "my message")

getCaesarR :: Handler Html
getCaesarR = do
  ((result, widget), enctype) <- runFormGet caesarForm
  let resultText = case result of
        FormSuccess caesar -> codeCaesar caesar
        _ -> ""
  defaultLayout $ do
    myCss
    [whamlet| 
      <h1> Caesar
      <form method=get action=@{CaesarR} enctype=#{enctype}>
        ^{widget}
        <button> Submit
      <p.result> result : #{resultText}
      ^{myFooter}
    |]

-- Music

musicForm :: Html -> MForm Handler (FormResult (Maybe T.Text), Widget)
musicForm extra = do
  (searchRes, searchView) <- mopt textField "searchtitle" Nothing
  let widget = do
        [whamlet|
          #{extra}
          <p>
            search in title : ^{fvInput searchView}
            <input type=submit>
        |]
  return (searchRes, widget)

getMusicR = do
  ((res, widget), enctype) <- runFormGet musicForm
  let result = case res of
        FormSuccess maybeStr -> maybe "" id maybeStr
        _ -> ""
  defaultLayout $ do
    titles <- liftIO $ selectTitles result
    myCss
    [whamlet|
      <h1> Music
      <form method=get action=@{MusicR} enctype=#{enctype}>
        ^{widget}
      <ul.result> 
        $forall l <- titles
          <li> #{l}<br>
      ^{myFooter}
      |]

-- main

main = warp port App

