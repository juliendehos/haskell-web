{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import           Data.Aeson (ToJSON)
import qualified Clay as C
import           Control.Monad.Trans (liftIO)
import qualified Data.ByteString.Char8 as B8
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           GHC.Generics (Generic)
import           Lucid
import           Network.Wai.Middleware.Cors (simpleCors)
import           Web.Scotty (get, html, json, middleware, scotty)

data Music = Music T.Text T.Text deriving (Show, Generic)

instance ToJSON Music

myMusics :: [Music]
myMusics = 
    [ Music "Paranoid android" "Radiohead"
    , Music "Just" "Radiohead"
    , Music "Take the power back" "Rage against the machine"
    , Music "How could I just kill a man" "Rage against the machine"
    , Music "La porte bonheur" "Ibrahim Maalouf"
    ]

bodyCss :: C.Css
bodyCss = C.body C.? C.backgroundColor C.beige

main :: IO ()
main = do
    let port = 3000

    scotty port $ do

        middleware simpleCors

        get "/" $ do
            let formatMusic :: Music -> T.Text
                formatMusic (Music artist title) = 
                    T.concat [artist, T.pack " - ", title]
            html $ renderText $ doctypehtml_ $ do
                header_ $ do
                    title_ "Musicapp"
                    style_ $ L.toStrict $ C.render bodyCss
                body_ $ do
                    h1_ "Musicapp"
                    div_ $ do
                        "My music:"
                        ul_ $ mapM_ (li_ . toHtml . formatMusic) myMusics
                    p_ $ a_ [href_ "raw"] "get raw data"
                    p_ $ a_ [href_ "json"] "get json data"

        get "/raw" $ html $ L.fromStrict $ T.pack $ show myMusics

        get "/json" $ json myMusics

