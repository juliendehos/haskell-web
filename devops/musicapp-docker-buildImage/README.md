
```
nix-build musicapp.nix --argstr createdAt $(date --utc +%FT%TZ) 
docker load < result
docker run --rm -p 3000:3000 juliendehos/test:musicapp
docker rmi juliendehos/test:musicapp 
```

