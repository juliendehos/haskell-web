{ pkgs ?  import <nixpkgs> {} , createdAt }:

with pkgs;

let
  _musicapp = import ./musicapp/default.nix {};
in

dockerTools.buildImage {
  name = "juliendehos/test";
  tag = "musicapp";
  contents = [ _musicapp ];
  created = createdAt;
  config = {
    Cmd = [ "${_musicapp}/bin/musicapp" ];
  };
}

