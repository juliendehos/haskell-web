{ pkgs ? import <nixpkgs> {} }:

let 

  app = pkgs.haskellPackages.callCabal2nix "musicapp" ./. {};

in 

pkgs.stdenv.mkDerivation {
  name = "musicapp";
  src = ./.;
  installPhase = ''
    mkdir -p $out/bin
    cp ${app}/bin/* $out/bin/
    cp music.sql $out/bin
  '';
}

