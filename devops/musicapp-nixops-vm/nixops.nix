{
  network.description = "musicapp";

  webserver = { config, pkgs, ... }: 
  let
    _musicapp = import ./musicapp/default.nix { inherit pkgs; };
  in
  {
    networking.firewall.allowedTCPPorts = [ 80 ];

    systemd.services.musicapp = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" "postgresql.service" ];
      script = ''
        export DATABASE_URL="host=localhost port=5432 dbname=musicdb user=musicuser password='toto'"
        export PORT=80
        cd ${_musicapp}/bin
        ./musicapp
      '';
    };

    services.postgresql = {
      enable = true;
      initialScript = "${_musicapp}/bin/music.sql";
      authentication = "local all all trust";
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}

