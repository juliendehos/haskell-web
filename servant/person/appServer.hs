{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

import Data.Aeson
import Data.Text
import GHC.Generics
import Network.Wai.Handler.Warp
import Servant

data Person = Person 
  { name :: Text
  , age :: Int 
  } deriving (Generic)

instance FromJSON Person

instance ToJSON Person

type MyApi = "person" 
  :> Capture "name" Text 
  :> Capture "age" Int 
  :> Get '[JSON] Person

myApi :: Proxy MyApi
myApi = Proxy

server :: Server MyApi
server name age = return $ (Person name age)

serverApp :: Application
serverApp = serve myApi server

main :: IO ()
main = run 8001 serverApp

