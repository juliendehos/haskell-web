{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

import Data.Aeson (FromJSON)
import Data.Proxy (Proxy(..))
import Data.Text (Text)
import GHC.Generics (Generic)
import Servant.API ((:>), (:<|>)(..), Get, JSON, Capture)
import Servant.Client (ClientM, ClientEnv(..), client, BaseUrl(..), Scheme(..), runClientM)
import Network.HTTP.Client (Manager, newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)

data Package = Package { packageName :: Text } deriving (Show, Generic)
instance FromJSON Package

data User = User { username :: Text, userid :: Int } deriving (Show, Generic)
instance FromJSON User

type HackageAPI 
  =    "packages" :> ".json" :> Get '[JSON] [Package]
  :<|> "users" :> ".json" :> Get '[JSON] [User]

hackageAPI :: Proxy HackageAPI
hackageAPI = Proxy

getPackages :: ClientM [Package]
getUsers :: ClientM [User]
getPackages :<|> getUsers = client hackageAPI 

url = BaseUrl Https "hackage.haskell.org" 443 ""

main :: IO ()
main = do
  manager' <- newManager tlsManagerSettings

  resPackages <- runClientM getPackages (ClientEnv manager' url)
  case resPackages of
    Left err -> putStrLn $ "Error: " ++ show err
    Right res -> putStrLn $ "nb packages: " ++ show (length res)
    -- Right res -> mapM_ print res

  resUsers <- runClientM getUsers (ClientEnv manager' url)
  case resUsers of
    Left err -> putStrLn $ "Error: " ++ show err
    Right res -> putStrLn $ "nb users: " ++ show (length res)

