{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson (FromJSON)
import Data.Proxy (Proxy(..))
import Data.Text (Text)
import GHC.Generics (Generic)
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant.API ((:>), Get, JSON, QueryParam)
import Servant.Client (ClientM, ClientEnv(..), client, BaseUrl(..), Scheme(..), runClientM)

data MyTimezone = MyTimezone
  { dstOffset :: Int
  , rawOffset :: Int
  , status :: Text
  , timeZoneId :: Text
  , timeZoneName :: Text
  } deriving (Show, Generic)

instance FromJSON MyTimezone

type TimezoneApi 
  = "json" 
    :> QueryParam "location" Text 
    :> QueryParam "timestamp" Int 
    :> Get '[JSON] MyTimezone

timezoneApi :: Proxy TimezoneApi
timezoneApi = Proxy

getTimezone :: Maybe Text -> Maybe Int -> ClientM MyTimezone
getTimezone = client timezoneApi 

query :: ClientM (MyTimezone, MyTimezone)
query = do
  france <- getTimezone (Just "50,2") (Just 0)
  japon <- getTimezone (Just "38,140") (Just 0)
  return (france, japon)

url :: BaseUrl
url = BaseUrl Https "maps.googleapis.com" 443 "/maps/api/timezone"

main :: IO ()
main = do
  manager <- newManager tlsManagerSettings

  resTimezone <- runClientM query (ClientEnv manager url)
  case resTimezone of
    Left err -> putStrLn $ "Error: " ++ show err
    Right (resFrance, resJapon) -> do
      print resFrance
      print resJapon

