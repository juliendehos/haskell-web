{ pkgs ? import <nixpkgs> {} }:
let
  result = import (pkgs.fetchFromGitHub {
    owner = "dmjio";
    repo = "miso";
    sha256 = "18c9w3p19crf7ks8aygy1x016jsibvz9g6wmvdgbbg7rbcq7rd0p";
    rev = "e2bd6765b520ffc03830053c52eb0ffca4edabcd";
  }) {};
in 
  (pkgs.haskell.packages.ghcjs.callPackage ./app.nix {
    miso = result.miso-ghcjs;
  }).overrideDerivation (drv: {
    postInstall = ''
      cp ${drv.src}/index.html $out/bin/app.jsexe/
    '';
  })

