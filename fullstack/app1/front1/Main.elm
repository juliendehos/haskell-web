module Main exposing (..)

import Html exposing (..)
import Html.Events exposing (onClick, onInput)


main =
    beginnerProgram { model = model, update = update, view = view }


type alias Model =
    { tasks : List String
    , entry : String
    }


model : Model
model =
    Model [] ""


type Msg
    = NewTask
    | NewEntry String


update : Msg -> Model -> Model
update msg model =
    case msg of
        NewTask ->
            { model
                | tasks =
                    if model.entry == "" then
                        model.tasks
                    else
                        [ model.entry ] ++ model.tasks
            }

        NewEntry str ->
            { model | entry = str }


view model =
    div []
        [ h1 []
            [ text "ma page web" ]
        , div
            []
            [ input
                [ onInput NewEntry ]
                []
            , button [ onClick NewTask ]
                [ text "ajouter" ]
            , ul []
                (List.map (\x -> li [] [ text x ]) model.tasks)
            ]
        ]
