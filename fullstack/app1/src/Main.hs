{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty (middleware, scotty, get, file, param)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)

main :: IO ()
main = scotty 3000 $ do
  middleware logStdoutDev

  -- TODO index dynamique

  get "/" $ file $ "./static/index.html"

  get "/build/:myfile" $ do
    myfile <- param "myfile"
    file $ "./build/" ++ myfile

  get "/:myfile" $ do
    myfile <- param "myfile"
    file $ "./static/" ++ myfile

